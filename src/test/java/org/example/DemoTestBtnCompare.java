package org.example;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

import java.io.File;

public class DemoTestBtnCompare {
    private static WebDriver driver;
    private static File webpage;

    /**
     * осуществление первоначальной настройки
     */
    @BeforeClass
    public static void setup() {
        //определение пути до драйвера и его настройка
        System.setProperty("webdriver.chrome.driver", MyConfProperties.getProperty("chromedriver"));
        //создание экземпляра драйвера
        driver = new ChromeDriver();
        webpage = new File("src/test/resources/buttoncounter.html");
    }

    @Test
    public void buttonCounterTest1() {
        driver.get(webpage.getAbsolutePath());

        String buttonName = driver.findElement(By.id("clicks")).getAttribute("value");
        Assert.assertEquals(buttonName, "1");
    }

    @Test
    public void buttonCounterTest2() {
        driver.get(webpage.getAbsolutePath());

        WebElement button = driver.findElement(By.id("clicks"));
        button.click();
        String buttonName = driver.findElement(By.id("clicks")).getAttribute("value");
        Assert.assertEquals(buttonName, "2");
    }

    /**
     * осуществление выхода из аккаунта с последующим закрытием окна браузера
     */
    @AfterClass
    public static void tearDown() {
        driver.quit();
    }
}
package org.example;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

import java.io.File;
import java.util.concurrent.TimeUnit;

public class DemoTestScrool {
    private static WebDriver driver;
    private static File webpage;

    /**
     * осуществление первоначальной настройки
     */
    @BeforeClass
    public static void setup() {
        //определение пути до драйвера и его настройка
        System.setProperty("webdriver.chrome.driver", MyConfProperties.getProperty("chromedriver"));
        //создание экземпляра драйвера
        driver = new ChromeDriver();
        webpage = new File("src/test/resources/100_buttons.html");
        JavascriptExecutor js = (JavascriptExecutor) driver;
    }

    @Test
    public void buttonCounterTest1() {
        driver.get(webpage.getAbsolutePath());
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
        WebElement lastElement = driver.findElement(By.xpath("(//*)[last()]"));
        boolean displayed = lastElement.isDisplayed();
        System.out.println(displayed);
        Assert.assertEquals(lastElement.getAttribute("value"), "100");
    }


    /**
     * осуществление выхода из аккаунта с последующим закрытием окна браузера
     */
    @AfterClass
    public static void tearDown() {
        driver.quit();
    }
}
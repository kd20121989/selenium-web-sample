package org.example;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

import java.io.File;

public class DemoTestInvisBtn {
    private static WebDriver driver;
    private static File webpage;

    /**
     * осуществление первоначальной настройки
     */
    @BeforeClass
    public static void setup() {
        //определение пути до драйвера и его настройка
        System.setProperty("webdriver.chrome.driver", MyConfProperties.getProperty("chromedriver"));
        //создание экземпляра драйвера
        driver = new ChromeDriver();
        webpage = new File("src/test/resources/invis_btn.html");
    }

    @Test
    public void buttonCounterTest1() {
        driver.get(webpage.getAbsolutePath());

        String buttonStyle = driver.findElement(By.id("cant_see_me")).getAttribute("style");
        Assert.assertEquals(buttonStyle, "visibility: hidden;");
    }


    /**
     * осуществление выхода из аккаунта с последующим закрытием окна браузера
     */
    @AfterClass
    public static void tearDown() {
        driver.quit();
    }
}